const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2118582","root","password",{host:"127.0.0.1",dialect:"mysql"});
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.student = require("./models/Student.js")(sequelize);
db.grupa = require("./models/Grupa.js")(sequelize);
db.zadatak = require("./models/Zadatak.js")(sequelize);
db.vjezba = require("./models/Vjezba.js")(sequelize);

db.grupa.hasMany(db.student,{as:'studentiGrupe'});
db.vjezba.hasMany(db.zadatak,{as:'zadaciVjezbe'});

module.exports=db;