window.onload = function () {
    document.getElementById("brojStudenata").addEventListener("change", function () { dodajInputPolja() });
    document.getElementById("posalji").addEventListener("click", function () { dodajBatch() })
}
function dodajInputPolja() {
    let div = document.getElementById("container");
    let broj = document.getElementById("brojStudenata").value;
    StudentAjax.dodajInputPolja(div, broj);
}
function dodajBatch() {
    var msg = document.getElementById("tekst");
    let tekst = "";
    let container = document.getElementById("container");
    for (let i = 1; i < container.childElementCount; i = i + 8) {
        tekst += container.childNodes[i + 2].value + ',';
        tekst += container.childNodes[i + 4].value + ',';
        tekst += container.childNodes[i].value.toString() + ',';
        tekst += container.childNodes[i + 6].value;
        if (i < container.childElementCount - 8) {
            tekst += '\n';
        }
    }
    StudentAjax.dodajBatch(tekst, function (error, data) {
        let broj = document.getElementById("brojStudenata");
        if (data.hasOwnProperty('status')) {
            broj.value=0;
            dodajInputPolja();            
            msg.innerHTML = data.status;
        }
    });
}