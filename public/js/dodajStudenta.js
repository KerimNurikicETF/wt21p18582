window.onload=function(){
    document.getElementById("posalji").addEventListener("click",function(){dodajStudenta()})
}

function dodajStudenta(){
    let div = document.getElementById('tekst');
    let ime = document.getElementById('ime').value;
    let prezime = document.getElementById('prezime').value;
    let index = document.getElementById('index').value;
    let grupa = document.getElementById('grupa').value;
    let object = {
        ime:ime,
        prezime:prezime,
        index:index,
        grupa:grupa
    };
    
    StudentAjax.dodajStudenta(object,function(error,data){
        if(data.hasOwnProperty('status'))
            div.innerHTML = data.status;
    });
}