let assert = chai.assert;
var test1 = "{\"stats\":{\"tests\":3,\"passes\":1,\"failures\":2},\"tests\":[{\"fullTitle\":\"T1\"}, {\"fullTitle\":\"T2\"},{\"fullTitle\":\"T3\"}],\"failures\":[{\"fullTitle\":\"T2\"},{\"fullTitle\":\"T3\"}]}";
var test2 = "{\"stats\":{\"tests\":3,\"passes\":2,\"failures\":1},\"tests\":[{\"fullTitle\":\"T1\"}, {\"fullTitle\":\"T2\"},{\"fullTitle\":\"T3\"}],\"failures\":[{\"fullTitle\":\"T3\"}]}";
var test3 = "{\"stats\":{\"tests\":2,\"passes\":2,\"failures\":0},\"tests\":[{\"fullTitle\":\"T1\"}, {\"fullTitle\":\"T2\"}],\"failures\":[]}";
var test4 = "{\"stats\":{\"tests\":3,\"passes\":2,\"failures\":1},\"tests\":[{\"fullTitle\":\"T1\"}, {\"fullTitle\":\"T2\"}, {\"fullTitle\":\"T4\"}],\"failures\":[{\"fullTitle\":\"T4\"}]}";
var test5 = "{\"stats\":{\"tests\":4,\"passes\":2,\"failures\":2},\"tests\":[{\"fullTitle\":\"T1\"}, {\"fullTitle\":\"T2\"}, {\"fullTitle\":\"T3\"}, {\"fullTitle\":\"T4\"}],\"failures\":[{\"fullTitle\":\"T3\"},{\"fullTitle\":\"T4\"}]}";
var test6 = "{\"stats\":{\"tests\":4,\"passes\":1,\"failures\":3},\"tests\":[{\"fullTitle\":\"T4\"}, {\"fullTitle\":\"T2\"}, {\"fullTitle\":\"T3\"}, {\"fullTitle\":\"T1\"}],\"failures\":[{\"fullTitle\":\"T4\"},{\"fullTitle\":\"T1\"},{\"fullTitle\":\"T3\"}]}";
var test7 = "{\"stats\":{\"tests\":2,\"passes\":0,\"failures\":2},\"tests\":[{\"fullTitle\":\"A2\"}, {\"fullTitle\":\"A1\"}],\"failures\":[{\"fullTitle\":\"A2\"},{\"fullTitle\":\"A1\"}]}";

describe('TestoviParser', function () {
    describe('porediRezultate()', function () {
        it('Identicni testovi', function () {
            var rez = TestoviParser.porediRezultate(test1, test2);
            assert.equal(rez.tacnost, "66.7%", "Tacnost je 33.3%");
            assert.deepEqual(rez.greske, ["T3"]);
        });

        it('pojavljuje se samo u prvom', function () {
            var rez = TestoviParser.porediRezultate(test2, test3);
            assert.equal(rez.tacnost, "33.33333333333333%", "Tacnost je 33.3%");
            assert.deepEqual(rez.greske, ["T3"]);
        });

        it('jedan u prvom i jedan u drugom padaju(razliciti)', function () {
            var rez = TestoviParser.porediRezultate(test2, test4);
            assert.equal(rez.tacnost, "50%", "Tacnost je 50%");
            assert.deepEqual(rez.greske, ["T3", "T4"]);
        });

        it('Ispravljen 1 test iz prvog, jedan novi iz drugog', function () {
            var rez = TestoviParser.porediRezultate(test1, test5);
            assert.equal(rez.tacnost, "50%", "Tacnost je 50%");
            assert.deepEqual(rez.greske, ["T3", "T4"]);
        });

        it('Sortiranje identicni', function () {
            var rez = TestoviParser.porediRezultate(test6, test6);
            assert.equal(rez.tacnost, "25%", "Tacnost je 25%");
            assert.deepEqual(rez.greske, ["T1", "T3", "T4"]);
        });

        it('Sortiranje razliciti', function () {
            var rez = TestoviParser.porediRezultate(test6, test7);
            assert.equal(rez.tacnost, "100%", "Tacnost je 100%");
            assert.deepEqual(rez.greske, ["T1", "T3", "T4", "A1", "A2"]);
        });

    });
});