var TestoviParser = (function () {
    var dajTacnost = function (jsonString) {
        try {
            var myObj = JSON.parse(jsonString);
        } catch (e) {
            return JSON.parse('{"tacnost": "0%", "greske":[]}');
        }
        var prolaznost = Math.round(myObj.stats.passes / myObj.stats.tests * 1000) / 10;
        var array = []
        for (let x of myObj.failures) {
            array.push(x.fullTitle);
        }
        return { tacnost: prolaznost.toString() + "%", greske: array };
    }

    var porediRezultate = function (rezultat1, rezultat2) {
        var rez1 = JSON.parse(rezultat1);
        var rez2 = JSON.parse(rezultat2);
        function compare(a, b) {
            if (a.fullTitle < b.fullTitle) {
                return -1;
            }
            if (a.fullTitle > b.fullTitle) {
                return 1;
            }
            return 0;
        }
        function uRez2(x) {
            return rez2.tests.some(e => e.fullTitle === x.fullTitle);
        };
        var greske = [];
        var promjena;
        if (rez1.tests.every(uRez2) && rez1.stats.tests==rez2.stats.tests) {
            promjena = dajTacnost(rezultat2).tacnost;
            greske = rez2.failures;
            greske.sort(compare);
        } else {
            var padajuRez1 = 0;
            rez1.failures.forEach(element => {
                if (!uRez2(element)) {
                    padajuRez1 = padajuRez1 + 1;
                    greske.push(element);
                }
            });
            var promjenax = (padajuRez1 + rez2.stats.failures) / (padajuRez1 + rez2.stats.tests) * 100;
            promjena = promjenax.toString() + "%";
            greske.sort(compare);
            var rez2Greske = rez2.failures;
            rez2Greske.sort(compare);
            greske = greske.concat(rez2Greske);
        }
        var array = [];
        for (var y of greske) {
            array.push(y.fullTitle);
        }
        return { tacnost: promjena, greske: array }
    }

    return {
        dajTacnost: dajTacnost,
        porediRezultate: porediRezultate
    }
}());


