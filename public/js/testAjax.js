let assert = chai.assert
chai.should();

describe('MyAPI', function () {
    beforeEach(function () {
        this.xhr = sinon.useFakeXMLHttpRequest();

        this.requests = [];
        this.xhr.onCreate = function (xhr) {
            this.requests.push(xhr);
        }.bind(this);
    });

    afterEach(function () {
        this.xhr.restore();
    });

    it('Posalji validne podatke', function () {
        let podaci = { brojVjezbi: 4, brojZadataka: [1, 2, 2, 4] };
        let dataJson = JSON.stringify(podaci);
        VjezbeAjax.posaljiPodatke(podaci, function () { })
        this.requests[0].requestBody.should.equal(dataJson);
    });
    
    it('Dohvati validne podatke ',function(done){
        let podaci = { brojVjezbi: 4, brojZadataka: [1, 2, 3, 4] };
        var dataJson = JSON.stringify(podaci);
        VjezbeAjax.dohvatiPodatke(function(error,data){
        data.should.deep.equal(podaci);
        done();
        });
        this.requests[0].respond(200,{'Content-Type':'text/json'}, dataJson);
    });
    
    it('Dodaj 4 input Polja (4 labele, 4 input polja)',function(){
        let inputPolja = document.getElementById("inputPolja");
        VjezbeAjax.dodajInputPolja(inputPolja,4);
        assert.equal(inputPolja.childElementCount,8);
        assert.equal(inputPolja.getElementsByTagName("input").length,4);
        assert.equal(inputPolja.getElementsByTagName("label").length,4);
    })

    it('Dodaj 16 input Polja (ne treba nista iscrtati)',function(){
        let inputPolja = document.getElementById("inputPolja");
        VjezbeAjax.dodajInputPolja(inputPolja,16);
        assert.equal(inputPolja.childElementCount,0);
    })

    it('Iscrtaj vjezbe, te iscrtaj razlicite zadatke 2 put',function(){
        let podaci = { brojVjezbi: 4, brojZadataka: [1, 2, 3, 4] }; 
        let vjezbe = document.getElementById("vjezbe");
        VjezbeAjax.iscrtajVjezbe(vjezbe,podaci);
        assert.equal(vjezbe.childElementCount,4);
        assert.equal(vjezbe.getElementsByTagName("button").length,4);
        VjezbeAjax.iscrtajZadatke(vjezbe.childNodes[0],1);
        let zadaci = vjezbe.getElementsByTagName("DIV");
        assert.equal(zadaci.length,1);
        VjezbeAjax.iscrtajZadatke(vjezbe.childNodes[1],2);
        let zadaci2 = vjezbe.getElementsByTagName("DIV");
        assert.equal(zadaci.length,2);
        assert.equal(zadaci[0].style.display,"none");
        assert.equal(zadaci[1].style.display,"flex");
        
    })
});