var StudentAjax = (function () {
    var dodajStudenta = function(student,fnCallBack){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var jsonRes = JSON.parse(ajax.responseText);
                fnCallBack(null, jsonRes);
            }
            else if (ajax.readyState == 4) {
                fnCallBack(ajax.statusText, null);
            }
        }
        ajax.open("PUT", "http://localhost:3000/student", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(student));
    }

    var postaviGrupu = function(index,grupaObjekat,fnCallBack){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var jsonRes = JSON.parse(ajax.responseText);
                fnCallBack(null, jsonRes);
            }
            else if (ajax.readyState == 4) {
                fnCallBack(ajax.statusText, null);
            }
        }
        ajax.open("PUT", "http://localhost:3000/student/"+index.toString(), true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(grupaObjekat));
    }

    var dodajBatch = function(csvStudenti,fnCallBack){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var jsonRes = JSON.parse(ajax.responseText);
                fnCallBack(null, jsonRes);
            }
            else if (ajax.readyState == 4) {
                fnCallBack(ajax.statusText, null);
            }
        }
        ajax.open("POST", "http://localhost:3000/batch/student", true);
        ajax.setRequestHeader("Content-Type", "text/plain");
        ajax.send(csvStudenti);
    }

    var dodajInputPolja = function (DOMelementDIVauFormi, brojStudenata) {
        if (brojStudenata > 20 || brojStudenata < 1) {
            while (DOMelementDIVauFormi.firstChild) {
                DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.firstChild);
            }
            return;
        }

        if (brojStudenata*8 > DOMelementDIVauFormi.childElementCount)
            while (brojStudenata*8 != DOMelementDIVauFormi.childElementCount) {
                let inputNumbers = DOMelementDIVauFormi.getElementsByTagName("input").length;
                let input = document.createElement("input");
                let label = document.createElement("label");
                label.setAttribute("class","grid-items");
                input.setAttribute("type", "number");
                input.setAttribute("name", "index" + inputNumbers.toString());
                input.setAttribute("id", "index" + inputNumbers.toString());
                label.setAttribute("for",input.getAttribute("id"));
                label.innerHTML="Index"

                
                let input1 = document.createElement("input");
                let label1 = document.createElement("label");
                label1.setAttribute("class","grid-items");
                input1.setAttribute("type", "text");
                input1.setAttribute("name", "ime" + inputNumbers.toString());
                input1.setAttribute("id", "ime" + inputNumbers.toString());
                label1.setAttribute("for",input.getAttribute("id"));
                label1.innerHTML="Ime";

                
                let input2 = document.createElement("input");
                let label2 = document.createElement("label");
                label2.setAttribute("class","grid-items");
                input2.setAttribute("type", "text");
                input2.setAttribute("name", "prezime" + inputNumbers.toString());
                input2.setAttribute("id", "prezime" + inputNumbers.toString());
                label2.setAttribute("for",input.getAttribute("id"));
                label2.innerHTML='Prezime';

                
                let input3 = document.createElement("input");
                let label3 = document.createElement("label");
                label3.setAttribute("class","grid-items");
                input3.setAttribute("type", "text");
                input3.setAttribute("name", "grupa" + inputNumbers.toString());
                input3.setAttribute("id", "grupa" + inputNumbers.toString());
                label3.setAttribute("for",input.getAttribute("id"));
                label3.innerHTML="Grupa";
                
                DOMelementDIVauFormi.appendChild(label);
                DOMelementDIVauFormi.appendChild(input);
                DOMelementDIVauFormi.appendChild(label1);
                DOMelementDIVauFormi.appendChild(input1);
                DOMelementDIVauFormi.appendChild(label2);
                DOMelementDIVauFormi.appendChild(input2);
                DOMelementDIVauFormi.appendChild(label3);
                DOMelementDIVauFormi.appendChild(input3);
            }
        else {
            while (brojStudenata*8 != DOMelementDIVauFormi.childElementCount) {
                DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.lastChild);
                DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.lastChild);
                DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.lastChild);
                DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.lastChild);
                DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.lastChild);
                DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.lastChild);
                DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.lastChild);
                DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.lastChild);
            }   
        }
    }

    return {
        dodajStudenta: dodajStudenta,
        postaviGrupu: postaviGrupu,
        dodajBatch: dodajBatch,
        dodajInputPolja: dodajInputPolja
    }
}());