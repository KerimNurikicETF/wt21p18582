window.onload=function(){
    document.getElementById("brojVjezbi").addEventListener("change", function(){dodajInputPolja()});
    document.getElementById("posalji").addEventListener("click",function(){posaljiPodatke()})
}
function dodajInputPolja(){
    let div = document.getElementById("container");
    let broj=document.getElementById("brojVjezbi").value;
    VjezbeAjax.dodajInputPolja(div,broj);
}
function posaljiPodatke(){
    let brojVjezbi = document.getElementById("brojVjezbi").value;
    let brojZadataka = [];
    let container = document.getElementById("container");
    for(let i =1;i<container.childElementCount;i=i+2){
        brojZadataka.push(container.childNodes[i].value);
    }
    VjezbeAjax.posaljiPodatke({brojVjezbi:brojVjezbi,brojZadataka:brojZadataka},function(error,data){
        if(data.hasOwnProperty('status'))
            alert(data.data);
    });
}