var VjezbeAjax = (function () {
    var dodajInputPolja = function (DOMelementDIVauFormi, brojVjezbi) {
        if (brojVjezbi > 15 || brojVjezbi < 1) {
            while (DOMelementDIVauFormi.firstChild) {
                DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.firstChild);
            }
            return;
        }

        if (brojVjezbi > DOMelementDIVauFormi.childElementCount)
            while (brojVjezbi*2 != DOMelementDIVauFormi.childElementCount) {
                let inputNumbers = DOMelementDIVauFormi.getElementsByTagName("input").length;
                let input = document.createElement("input");
                let label = document.createElement("label");
                input.setAttribute("type", "number");
                input.setAttribute("value", 4);
                input.setAttribute("name", "z" + inputNumbers.toString());
                input.setAttribute("id", "z" + inputNumbers.toString());
                label.setAttribute("for",input.getAttribute("id"));
                label.innerHTML=input.getAttribute("name");
                DOMelementDIVauFormi.appendChild(label);
                DOMelementDIVauFormi.appendChild(input);
            }
        else {
            while (brojVjezbi*2 != DOMelementDIVauFormi.childElementCount) {
                DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.lastChild);
                DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.lastChild);
            }   
        }
    }

    var posaljiPodatke = function (vjezbeObjekat, callbackFja) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var jsonRes = JSON.parse(ajax.responseText);
                callbackFja(null, jsonRes);
            }
            else if (ajax.readyState == 4) {
                callbackFja(ajax.statusText, null);
            }
        }
        ajax.open("POST", "http://localhost:3000/vjezbe", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(vjezbeObjekat));
    }

    var dohvatiPodatke = function (callbackFja) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var jsonRes = JSON.parse(ajax.responseText);
                callbackFja(null, jsonRes);
            }
            else if (ajax.readyState == 4) {
                callbackFja(ajax.statusText, null);
            }
        }
        ajax.open("GET", "http://localhost:3000/vjezbe", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send();
    }

    var iscrtajVjezbe = function (divDOMelement, {brojVjezbi,brojZadataka}) {
        for (let i = 0; i < brojVjezbi; ++i) {
            let vjezba = document.createElement("button");
            vjezba.setAttribute("id", "vjezba" + (i + 1).toString());
            vjezba.addEventListener("click", function () { iscrtajZadatke(vjezba, brojZadataka[i]) });
            vjezba.innerHTML = "Vjezba " + (i + 1).toString();
            divDOMelement.appendChild(vjezba);
        }
    }

    var iscrtajZadatke = function (vjezbaDOMelement, brojZadataka) {
        let id = vjezbaDOMelement.getAttribute("id") + "div";
        var zadaci = document.getElementById(id);
        if (zadaci != null) {
            if (zadaci.style.display === "none") {
                zadaci.style.display = "flex";
            }
            else {
                zadaci.style.display = "none";
            }
        }
        else {
            zadaci = document.createElement("div");
            zadaci.setAttribute("class", "sub-btn");
            zadaci.setAttribute("id", id);
            zadaci.style.display="flex";
            vjezbaDOMelement.parentNode.insertBefore(zadaci, vjezbaDOMelement.nextSibling);
            for (let i = 0; i < brojZadataka; ++i) {
                let dugme = document.createElement("button");
                dugme.innerHTML = "Zadatak " + (i + 1).toString();
                zadaci.appendChild(dugme);
            }
        }
        let siblings = [];
        if (!vjezbaDOMelement.parentNode) {
            return;
        }
        let sibling = vjezbaDOMelement.parentNode.firstElementChild;
        do {
            if (sibling != vjezbaDOMelement.nextSibling && sibling.tagName=="DIV") {
                siblings.push(sibling);
            }
        } while (sibling = sibling.nextElementSibling);
        for (let i = 0; i < siblings.length; ++i) {
            siblings[i].style.display = "none";
        }
    }

    return {
        dodajInputPolja: dodajInputPolja,
        posaljiPodatke: posaljiPodatke,
        dohvatiPodatke: dohvatiPodatke,
        iscrtajVjezbe: iscrtajVjezbe,
        iscrtajZadatke: iscrtajZadatke
    }
}());