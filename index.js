const express = require("express");
const bodyParser = require('body-parser');
const fs = require('fs');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(express.static('./public/css'));
app.use(express.static('./public/js'));
app.use(express.static('./public/images'));
app.use(express.static('./public/json'));
app.use(express.static('./public/gspec'));
app.use(express.static('./public/html'));
app.use(express.static('public'));


const db = require('./db.js');

db.sequelize.sync().then(function () {
    console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
});


app.get('/vjezbe/', function (req, res) {
    let brojVjezbi = 0;
    let tasksNumber = [];
    res.setHeader('Content-Type', 'application/json');
    db.vjezba.findAll().then(function (vjezbe) {
        brojVjezbi = vjezbe.length;
        let promisi = [];
        for (const x of vjezbe) {
            promisi.push(x.getZadaciVjezbe().then(function (zadaci) {
                tasksNumber.push(parseInt(zadaci.length));
            }));
        }
        Promise.all(promisi).then(function (ok) {
            let object = {
                brojVjezbi: brojVjezbi,
                brojZadataka: tasksNumber
            }
            res.json(object);
            res.end();
        });
    }, function (neuspjeh) { console.log("nije ok") });
});


app.get('/unosVjezbi', function (req, res) {
    res.sendFile(__dirname + "/public/html/unosVjezbi.html");
});
app.get('/dodajStudenta', function (req, res) {
    res.sendFile(__dirname + "/public/html/dodajStudenta.html");
});
app.get('/postaviGrupu', function (req, res) {
    res.sendFile(__dirname + "/public/html/postaviGrupu.html");
});

app.post('/vjezbe', function (req, res) {
    let greske = [];
    let greska = "Pogrešan parametar ";
    let body = req.body;
    if (body.brojVjezbi < 1 || body.brojVjezbi > 15) {
        greske.push('brojVjezbi');
    }
    for (let i = 0; i < body.brojZadataka.length; ++i) {
        if (body.brojZadataka[i] > 10)
            greske.push('z' + i.toString())
    }
    if (body.brojZadataka.length != body.brojVjezbi) {
        greske.push('brojZadataka')
    }
    for (let i = 0; i < greske.length; ++i) {
        greska += greske[i];
        if (i != greske.length - 1)
            greska += ",";
    }
    if (greske.length != 0) {
        let object = {
            status: "error",
            data: greska
        }
        res.json(object);
        res.end();
        return;
    }
    let tekst = body.brojVjezbi.toString() + ',';
    for (let i = 0; i < body.brojZadataka.length; ++i) {
        tekst = tekst + body.brojZadataka[i];
        if (i != body.brojZadataka.length - 1)
            tekst += ',';
    }

    let promisiVjezbe = [];

    for (let i = 0; i < body.brojVjezbi; i++) {
        promisiVjezbe.push(db.vjezba.create({ naziv: 'Vjezba ' + (i + 1).toString() }).then(function (vjezba) {
            let promisi = [];
            for (let j = 0; j < body.brojZadataka[i]; j++) {
                promisi.push(db.zadatak.create({ naziv: 'Zadatak ' + (j + 1).toString() }).then(function (zadatak) {
                    vjezba.addZadaciVjezbe(zadatak);
                }));
            }
            Promise.all(promisi);
        }));
    }
    Promise.all(promisiVjezbe).then(function (ok) {
        res.json(req.body);
        res.end();
    })

});
app.put('/student', function (req, res) {
    const body = req.body;
    db.student.findOne({ where: { index: parseInt(body.index) } }).then(function (student) {
        if (student) {
            res.json({ status: 'Student sa indexom ' + body.index + ' već postoji!' });
            res.end();
        }
        else {
            db.grupa.findOne({ where: { naziv: body.grupa } }).then(function (grupa) {
                if (grupa) {
                    db.student.create({ ime: body.ime, prezime: body.prezime, index: parseInt(body.index) }).then(function (student) {
                        grupa.addStudentiGrupe(student).then(function () {
                            res.json({ status: 'Kreiran student!' });
                            res.end();
                        });
                    });
                }
                else {
                    db.grupa.create({ naziv: body.grupa }).then(function (grupa) {
                        db.student.create({ ime: body.ime, prezime: body.prezime, index: parseInt(body.index) }).then(function (student) {
                            grupa.addStudentiGrupe(student).then(function () {
                                res.json({ status: 'Kreiran student!' });
                                res.end();
                            });
                        });
                    });
                }
            });
        }
    });
});
app.put('/student/:index', function (req, res) {
    const body = req.body;
    const index = req.params["index"];
    db.student.findOne({ where: { index: parseInt(index) } }).then(function (student) {
        if (student) {
            db.grupa.findOne({ where: { naziv: body.grupa } }).then(function (grupa) {
                if (grupa) {
                    grupa.addStudentiGrupe(student).then(function () {
                        res.json({ status: 'Promjenjena grupa studentu ' + index });
                        res.end();
                    });
                }
                else {
                    db.grupa.create({ naziv: body.grupa }).then(function (grupa) {
                        grupa.addStudentiGrupe(student).then(function () {
                            res.json({ status: 'Promjenjena grupa studentu ' + index });
                            res.end();
                        });
                    });
                }
            });
        }
        else {
            res.json({ status: 'Student sa indexom ' + index + ' ne postoji!' });
            res.end();
        }
    });
});

app.post('/batch/student', function (req, res) {
    body = req.body;
    let arr1 = body.toString().split(/[\n\r]+/);
    let students = [];
    for (let i = 0; i < arr1.length; i++) {
        let row = arr1[i].split(',');
        let student = {
            ime: row[0],
            prezime: row[1],
            index: parseInt(row[2]),
            grupa: row[3]
        };
        students.push(student);
    }
    let studentiPromisi = [];
    var notAdded = [];
    for (let i = 0; i < students.length; i++) {
        studentiPromisi.push(db.student.findOne({ where: { index: parseInt(students[i].index) } }).then(function (student) {
            if (student) {
                notAdded.push(students[i].index.toString());
            }
            else {
                db.student.create({ ime: students[i].ime, prezime: students[i].prezime, index: parseInt(students[i].index) })
                    .then(function (student2) {
                        db.grupa.findOne({ where: { naziv: students[i].grupa } }).then(function (grupa) {
                            if (grupa) {
                                grupa.addStudentiGrupe(student2);
                            }
                            else {
                                db.grupa.create({ naziv: students[i].grupa }).then(function (grupa2) {
                                    grupa2.addStudentiGrupe(student2);
                                });
                            }
                        });
                    });
            }
        }));
    }

    Promise.all(studentiPromisi).then(function () {
        if (notAdded.length > 0) {
            var response = "Dodano " + (students.length - notAdded.length).toString() + " studenata, a studenti ";
            for (let i = 0; i < notAdded.length; i++) {
                response = response + notAdded[i];
                if (i != notAdded.length - 1)
                    response = response + ",";
            }
            response = response + " već postoje!"
            res.json({ status: response });
            res.end();
        }
        else {
            res.json({ status: "Dodano " + students.length + " studenata!" });
            res.end();
        }
    })
});


app.listen(3000);